#### CoyPu Ontology - COY

This is the CoyPu Ontology main repository. The most recent version of the COY ontology is located at /ontology/global/coy.ttl.

Guidelines on our naming policy and ontology creation can be found [here](https://gitlab.com/coypu-project/ontology-creation-policy). Please always create a new branch before making changes - never push to *Main*.
